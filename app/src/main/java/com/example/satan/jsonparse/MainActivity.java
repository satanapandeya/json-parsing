package com.example.satan.jsonparse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView printJson = (TextView) findViewById(R.id.json_id);

        //Reading Json File

        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;


        try {
            br = new BufferedReader(new InputStreamReader(getAssets().open("JsonData.json")));
            String tempJson;
            while ((tempJson = br.readLine()) != null)
                sb.append(tempJson);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

       //parse the json data


        String jsonData = " ";
        try {
            String stringJson = sb.toString();
//            Log.e("Tag",stringJson);
            JSONObject mainJsonObject = new JSONObject(stringJson);

            JSONArray jsonArray = mainJsonObject.getJSONArray("option");
//            Log.e("What is here","jsonArray");

            for(int i = 0; i < jsonArray.length();i++){

                JSONObject objectInsideArray = jsonArray.getJSONObject(i);
                int id = Integer.parseInt(objectInsideArray.getString("id"));
                String symbol = objectInsideArray.optString("symbol");

                jsonData += "Option : " + i + "\n" + "Id : " + id + "\n" + "Symbol : " + symbol + "\n" + "\n";
            }
            printJson.setText(jsonData);
        }
        catch (JSONException e){
            e.printStackTrace();
        }

    }
}
